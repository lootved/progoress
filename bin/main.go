package main

import (
	"math/rand"
	"sync"
	"time"

	"gitlab.com/lootved/progoress"
)

var pm *progoress.ProgressMonitor

func main() {

	nbrParralelTasks := 4
	nbrTasks := 10

	pm = progoress.NewProgressMonitor(nbrTasks).SetLineWidth(15)
	pm.StartMonitoring()
	sem := make(chan bool, nbrParralelTasks)
	var wg sync.WaitGroup

	wg.Add(nbrTasks)
	for i := 0; i < nbrTasks; i++ {
		sem <- true
		go func(taskId int) {
			doWork(taskId)
			<-sem
			wg.Done()
		}(i)
	}
	wg.Wait()

}

func doWork(id int) {
	nbrSubTasks := (1 + rand.Int()%5)
	pm.StartTask(id, nbrSubTasks)
	var wg sync.WaitGroup
	wg.Add(nbrSubTasks)
	for i := 0; i < nbrSubTasks; i++ {
		go func() {
			n := (2 + rand.Int()%3)
			time.Sleep(time.Duration(n) * time.Second)
			pm.FinishSubTask(id)
			wg.Done()
		}()
	}
	wg.Wait()
}
