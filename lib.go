package progoress

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type ProgressMonitor struct {
	mutex            sync.Mutex
	remainingTasks   int
	nbrTasks         int
	activeTasks      map[int]int
	subTasksSize     map[int]int
	progressLineWith int
}

func (pm *ProgressMonitor) SetLineWidth(width int) *ProgressMonitor {
	pm.progressLineWith = width
	return pm
}

func NewProgressMonitor(nbrTasks int) *ProgressMonitor {
	var pm ProgressMonitor
	pm.nbrTasks = nbrTasks
	pm.remainingTasks = nbrTasks
	pm.activeTasks = make(map[int]int)
	pm.subTasksSize = make(map[int]int)
	pm.progressLineWith = 20

	clearScreen()
	saveCursorPosition()
	return &pm
}

func (pm *ProgressMonitor) FinishSubTask(id int) {
	pm.mutex.Lock()
	pm.activeTasks[id]--
	if pm.activeTasks[id] == 0 {
		delete(pm.activeTasks, id)
		delete(pm.subTasksSize, id)
		pm.remainingTasks--
		if pm.remainingTasks == 0 {
			pm.drawProgress()
			//avoid overwriting the current line
			fmt.Println()
			fmt.Println("finished processing all tasks")
		}
	}
	pm.mutex.Unlock()
}

func (pm *ProgressMonitor) StartTask(id, nbrSubTasks int) {
	pm.activeTasks[id] = nbrSubTasks
	pm.subTasksSize[id] = nbrSubTasks
}

type msgType int

const (
	startGroup msgType = iota
	finishElem msgType = iota
)

type logMsg struct {
	kind  msgType
	grpid int
	val   int
}

func clearScreen() {
	fmt.Print("\033[2J\033[H")
}
func saveCursorPosition() {
	fmt.Print("\033[s")
}
func moveCursorToSavedPosition() {
	fmt.Print("\033[u")
}

func (pm *ProgressMonitor) StartMonitoring() {
	go pm.refreshScreen()
}

func (pm *ProgressMonitor) refreshScreen() {
	for {
		time.Sleep(200 * time.Millisecond)
		pm.drawProgress()
	}
}

func (pm *ProgressMonitor) drawProgress() {

	line := buildProgressStr(pm.nbrTasks-pm.remainingTasks, pm.nbrTasks, pm.progressLineWith, '#', '.')
	keys := make([]int, len(pm.activeTasks))
	i := 0
	for k := range pm.activeTasks {
		keys[i] = k
		i++
	}
	sort.Ints(keys)

	fmt.Println("Processed", line, "tasks done")
	for _, k := range keys {
		v := pm.activeTasks[k]
		subTaskSize := pm.subTasksSize[k]
		line = buildProgressStr(subTaskSize-v, subTaskSize, pm.progressLineWith, '#', '.')
		fmt.Println("  Task:", k, line)
	}
	moveCursorToSavedPosition()
}

func buildProgressStr(nbrFinished, nbrTasks, displayWidth int, doneChar, pendingChar byte) string {
	r := float32(displayWidth) * float32(nbrFinished) / float32(nbrTasks)
	n := int(r)
	sb := strings.Builder{}
	for i := 0; i < n; i++ {
		sb.WriteByte(doneChar)
	}
	for i := n; i < displayWidth; i++ {
		sb.WriteByte(pendingChar)
	}

	finished := strconv.Itoa(nbrFinished)
	tasks := strconv.Itoa((nbrTasks))
	sb.WriteString(" " + finished)
	sb.WriteString("/" + tasks)

	return sb.String()
}
